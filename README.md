## ECD - Projet - Playlists

Ce projet porte sur la fouille de données. 
Les données sont des playlists de spotify récupérer sous format JSON (cf. Rapport pour le format). 1 million de playliste sont représentées dans des sous dossiers Json de 100 playlists chaqu’un. Ces playlist ont été utilisé pour effectuer un clustering sur les différentes musiques, ainsi que des possible recommandatio basé sur ce que les écoutes personnels.

Structure du projet: 
``` bash
├── suggestion.py
├── Data
│   ├── Kmeans.plk
│   ├── mat.npz
│   ├── names.txt
│   ├── playlist_rap.json
│   ├── playlist_rock.json
│   └── playlist_spotify.json
└── Scripts
    ├── API_Spotify.ipynb
    ├── mknames.R
    ├── mksparsemat.R
    ├── pickels.ipynb
    └── Vectorizer.ipynb
    

```
Le projet s'articule en deux temps. 

##### 1.Entrainement
``` bash
                                         mksparsemat.r                      pickels.ipynb
Entrée : Million de playlists (json)  ──────────────────>  Sparse Matrice ──────────────────>  Sortie : modèle(Kmeans.plk)

```
Cette partie à déjà été traité et le fichier de sortie se trouve dans le dossier src du projet (Kmeans.plk).

##### 2.Suggestion

``` bash
                                      suggestion.py                           
Entrée : playlist.json (json)  ──────────────────>   Sortie : suggestion.txt 
```

Cette partie permets la mise en place d'une recomandation basé sur le jeux de données d'1 millions de playlist traité au préalable. 
En entrée on attend un fichier Json d'une ou plusieurs playlists utilisateur désireux d'avoir des recommandations musical basé sur ces musiques.  
Des fichiers tests (playlist_rap.json, playlist_rock.json, playlist_spotify.json) ont été placé dans ce projet, mais il est possible d'utiliser votre propore playlist si celle-ci est sous la forme suivante : 

``` bash
{
  "items": [
    {
      "track": {
        "album": {
          "name": "",
          "uri": ""
        },
        "artists": [
          {
            "name": "",
            "uri": ""
          }
        ],
        "name": "",
        "uri": ""
      }}]}
   
   ```
      
Les uri référencent respectivement l'album_uri, artists_uri et track_uri de spotify.
 
Si une playlist Spotify est déjà existante il est possible d'extraire celle-ci facilment via l'API Spotify. Un exemple de ce qui peut être fait pour récupérer les données a été ajouter dans le projet : API_Spotify.ipynb. Ce script permets de récupérer ses propres playlistes Spotify en indiquant son client_id et client_secret à récupérer sur spotify. Il donne en sortie le fichier Json déjà bien parsé pour être directement intégré au script suggestion.py pour effectuer une recommandation.

A la sortie du script suggestion.py un fichier texte (suggestion.txt) est retourner. Celui-ci contient le premières recommandations ,basé sur les musiques existantent du dataset de départ, sous forme de lien. Ces liens pointes directement vers les musique recommandé sur Spotify.

##### 3.Utilisation

Le script suggestion.py s'utilise depuis le terminal avec la ligne de commande suivante:
'''
suggestion.py pathToJSONPlaylist n
'''

Avec l'argument n optionel qui fixe le nombre de chansons à recommander (10 par défault)
un exemple de sortie pour le fichier playlist_rap.json est disponible dans le dossier Data/suggestion.txt
